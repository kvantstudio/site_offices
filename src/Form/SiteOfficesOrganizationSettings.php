<?php

namespace Drupal\site_offices\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure contact settings for one main organization.
 */
class SiteOfficesOrganizationSettings extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_offices_organization_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'site_offices.organization_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('site_offices.organization_settings');

    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Full organization name'),
      '#default_value' => $config->get('name'),
    );

    $form['addressCountry'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Country'),
      '#default_value' => $config->get('addressCountry'),
    );

    $form['addressRegion'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Region'),
      '#default_value' => $config->get('addressRegion'),
    );

    $form['addressLocality'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Address locality'),
      '#default_value' => $config->get('addressLocality'),
    );

    $form['postalCode'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Postal code'),
      '#default_value' => $config->get('postalCode'),
    );

    $form['streetAddress'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Street address'),
      '#default_value' => $config->get('streetAddress'),
    );

    $form['url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Site URL'),
      '#default_value' => $config->get('url'),
    );

    $form['faxNumber'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Fax number'),
      '#default_value' => $config->get('faxNumber'),
    );

    $form['telephone'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Phone number'),
      '#default_value' => $config->get('telephone'),
    );

    $form['email'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('E-mail'),
      '#default_value' => $config->get('email'),
    );

    $form['opening_hours'] = array(
      '#type' => 'details',
      '#title' => $this->t('Opening hours'),
      '#open' => TRUE,
    );

    $form['opening_hours']['datetime_value'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('datetime'),
      '#default_value' => $config->get('datetime_value'),
      '#description' => $this->t('Schema.org format. For example, Mo-Fr 07:00-23:00'),
    );

    $form['opening_hours']['datetime_text'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $config->get('datetime_text'),
    );

    $form['description'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $config->get('description'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('site_offices.organization_settings')
      ->set('name', trim($form_state->getValue('name')))
      ->set('addressCountry', trim($form_state->getValue('addressCountry')))
      ->set('addressRegion', trim($form_state->getValue('addressRegion')))
      ->set('addressLocality', trim($form_state->getValue('addressLocality')))
      ->set('postalCode', trim($form_state->getValue('postalCode')))
      ->set('streetAddress', trim($form_state->getValue('streetAddress')))
      ->set('url', trim($form_state->getValue('url')))
      ->set('faxNumber', trim($form_state->getValue('faxNumber')))
      ->set('telephone', trim($form_state->getValue('telephone')))
      ->set('email', trim($form_state->getValue('email')))
      ->set('datetime_value', trim($form_state->getValue('datetime_value')))
      ->set('datetime_text', trim($form_state->getValue('datetime_text')))
      ->set('description', trim($form_state->getValue('description')))
      ->save();
  }
}
