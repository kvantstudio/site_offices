<?php

namespace Drupal\site_offices\TwigExtension;

/**
 * Twig extension that adds a custom function and a custom filter.
 */
class SiteOfficesTwigExtension extends \Twig_Extension {

  /**
   * Generates a list of all Twig functions that this extension defines.
   *
   * @return array
   *   A key/value array that defines custom Twig functions. The key denotes the
   *   function name used in the tag, e.g.:
   *   @code
   *   {{ testfunc() }}
   *   @endcode
   *
   *   The value is a standard PHP callback that defines what the function does.
   */
  public function getFunctions() {
    return array(
      'getOrganizationSettings' => new \Twig_Function_Function(array('Drupal\site_offices\TwigExtension\SiteOfficesTwigExtension', 'getOrganizationSettings')),
      'getOrganization' => new \Twig_Function_Function(array('Drupal\site_offices\TwigExtension\SiteOfficesTwigExtension', 'getOrganization')),
    );
  }

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   *   A unique identifier for this Twig extension.
   */
  public function getName() {
    return 'site_offices.twig_extension';
  }

  /**
   * Настройки организации.
   */
  public static function getOrganizationSettings($name = 'all') {
    $config = \Drupal::config('site_offices.organization_settings')->get();
    if ($name == 'all') {
      return $config;
    } else {
      if (isset($config[$name])) {
        return $config[$name];
      }
    }
    return FALSE;
  }

  /**
   * Сниппет организации.
   */
  public static function getOrganization() {
    return array(
      '#theme' => 'site_offices_organization',
      '#attached' => array(
        'library' => array(
          'site_offices/module',
        ),
      ),
    );
  }
}
